
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <title>Students</title>
</head>
<body>
    <a href="{{ route('students.create') }}">create new</a>
    <table class="table">
        <thead>
            <th>Sl</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Date of Borth</th>
            <th>Action</th>
        </thead>
        <tbody>

            @php
                $sl = 0;
            @endphp
            @foreach ($students as $stu)
                <tr>
                    <td>{{ ++$sl }}</td>
                    <td>{{ $stu->name }}</td>
                    <td>{{ $stu->phone }}</td>
                    <td>{{ $stu->dob }}</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ route('students.edit', $stu->id) }}">Edit</a>
                        <a class="btn btn-danger btn-sm" href="{{ route('students.destroy', $stu->id) }}">Delete</a>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
</body>
</html>