<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <title>Create New Student</title>
</head>
<body>
    <form action="{{ route('students.update', $student->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <input type="text" name="name" placeholder="Full Name" value="{{ $student->name }}" required><br>
        <input type="text" name="phone" placeholder="Phone Number" value="{{ $student->phone }}" required><br>
        <input type="date" name="dob" value="{{ $student->dob }}" required>
        <input type="submit" value="Submit">
    </form>
</body>
</html>