<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <title>Create New Student</title>
</head>
<body>
    <form action="{{ route('students.store') }}" method="POST">
        @csrf
        <input type="text" name="name" placeholder="Full Name" required><br>
        <input type="text" name="phone" placeholder="Phone Number" required><br>
        <input type="date" name="dob" required>
        <input type="submit" value="Submit">
    </form>
</body>
</html>