<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Student;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $fake = Faker::create();
        
        for($i = 1; $i<=10; $i++){
            $student = new Student;
            $student->name = $fake->name;
            $student->phone = $fake->phoneNumber;
            $student->dob = $fake->date;
            $student->save();
        }
    }
}
